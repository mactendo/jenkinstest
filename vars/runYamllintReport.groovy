void call(Map parameters = [:]) {
    String stageName = 'Linting Report'
   
    // Yamllint configuration rules
    String configYamllint = libraryResource('yamllint-config/rules.yml')
    // Html header for the Yamllint report
    String header = libraryResource('yamllint-report-template/header.html')
    // Html tail for the Yamllint report
    String tail = libraryResource('yamllint-report-template/tail.html')
    // Artifactory VR URL
    String virtualRepositoryUrl = '<url>'
    // Installation cmd for yamllint
    //String installationCmd = "pip3 install -q -U --user yamllint --index-url $virtualRepositoryUrl"
    String installationCmd = "pip3 install -q -U --user yamllint"
    // Sed commands to add row and columns html tag to the Yamllint output data
    String sedCmds = "sed -i -e 's-^-<tr><td>-;s-: -</td><td>-;s-\$-</td></tr>-'"

    int yamllintFoundErrors = sh(returnStatus: true, script: """
            $installationCmd
            echo "$configYamllint" > .yamllint
            echo $pwd
            echo ${env.WORKSPACE}
            ls -ltr
            yamllint -f parsable . > yamllintReport.html
            """)

    sh(script: """
            echo "$header" > header.html
            echo "$tail" > tail.html
            $sedCmds yamllintReport.html
            cat header.html yamllintReport.html tail.html > index.html
            """)

    publishHTML(target: [
        allowMissing            : false,
        alwaysLinkToLastBuild   : true,
        keepAll                 : true,
        reportDir               : '.',
        reportFiles             : 'index.html',
        reportName              : stageName,
    ])

    if (yamllintFoundErrors != 0) {
        error 'There are errors in the Yamllint Report stage'
    }
    
}
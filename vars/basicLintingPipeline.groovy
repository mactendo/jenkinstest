void call(Map pipelineProperties = [:]) {
  String pipelineName = 'Basic Linting Pipeline'
  final String TRUNK_BRANCH_PROPERTY_NAME = 'trunkBranch'

  String agentLabel = 'controller'

  pipeline {
    agent {
      node {
        label agentLabel
      }
    }

    options {
      skipDefaultCheckout true
      timeout(time: 10, unit:'MINUTES')
      disableConcurrentBuilds()
    }

    stages {
      //stage('Clone') {
        //steps {
          //cloneSetEnvNotifyStart([displayName: pipelineName] + pipelineProperties)
        //}
      //}
      stage('Checkout') {
        steps {
          checkout([$class: 'GitSCM', 
            branches: [[name: '*/master']],
            doGenerateSubmoduleConfigurations: false,
            extensions: [[$class: 'CleanCheckout']],
            submoduleCfg: [],
            userRemoteConfigs: [[url: 'https://mactendo@bitbucket.org/mactendo/jenkinstest.git']]])
            sh "ls -ltr"
        }
      }

      stage('Lint') {
        steps {
          echo 'Linting goes here. Must support different type of linting e.eg. Ansible, Yaml, Sh, JS, etc...' 
          script {
            if (pipelineProperties.get('linterType').contains('yml')) {
              runYamllintReport(pipelineProperties)
            } 
            else {
              echo 'No valid lintertypes were found. Please look into the entered lintertypes!'
            }
          }
        }
      }
    }
  }
}